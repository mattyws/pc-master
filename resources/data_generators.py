import json
import os
from natsort import natsorted


class PatentDataGenerator(object):

    """
    A data generator for patent files filtered by the filter_complete_patent.py.
    The generator takes the path to the .json files and use it as generator.
    """

    def __init__(self, patent_directory, max_documents_per_file=100, loop_forever=True):
        """
        The class constructor
        :param patent_directory: the directory where lies the .json files.
        :param max_documents_per_file: how many patent documents is in each file, it is used to only to calculate the
                len for the generator
        :param loop_forever: if the generator has to loop forever through the data

        """
        self.patent_directory = patent_directory
        self.max_documents_per_file = max_documents_per_file
        self.loop_forever = loop_forever
        # Sort the json files in a natural sort
        self.json_files = natsorted(os.listdir(patent_directory))
        # Beginning the control variables
        self.file_pos = 0 # The position in the json_files array
        self.json_content = [] # the content of the json_file[file_pos]
        self.content_pos = 0 # The position in json_content array

    def get_json_file_name(self):
        """
        Return the .json file name that the generator is current loaded.
        The loaded json content is aways the file_pos-1.
        :return: the .json file name
        """
        # In case that someone calls for it when the class didn't load any documents yet.
        if self.file_pos != 0:
            return self.json_files[self.file_pos-1]
        else:
            return self.json_files[self.file_pos]

    def advance_json_file(self):
        self.file_pos += 1
        self.content_pos = 0

    def __load_json_document(self):
        if self.file_pos >= len(self.json_files) and self.loop_forever:
            self.file_pos = 0
        elif self.file_pos >= len(self.json_files) and not self.loop_forever:
            raise StopIteration()
        with open(self.patent_directory+'/'+self.json_files[self.file_pos], 'r') as f:
            self.json_content = json.load(f)
        self.advance_json_file()

    def __len__(self):
        return len(self.json_files)*self.max_documents_per_file

    def __next__(self):
        if len(self.json_content) == 0 or self.content_pos >= len(self.json_content):
            self.__load_json_document()
        document = self.json_content[self.content_pos]
        self.content_pos += 1
        return document

    def __iter__(self):
        return self