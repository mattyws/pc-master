import numpy as np
from gensim.models import Word2Vec, KeyedVectors


class Word2VecTrainer(object):
    """
    Perform training, save and load gensim word2vec
    """

    def __init__(self, min_count=2, size=200, workers=4, window=3, iter=10):
        self.min_count = min_count
        self.size = size
        self.workers = workers
        self.window = window
        self.iter = iter
        self.model = None

    def train(self, corpus, sg=0):
        self.model = Word2Vec(corpus, min_count=self.min_count, size=self.size, workers=self.workers, window=self.window, iter=self.iter, sg=sg)

    def save(self, filename):
        self.model.save(filename)

    def get_model(self):
        return self.model

    def load_model(self, filename):
        return Word2Vec.load(filename)

    def load_google_model(self, filename):
        return KeyedVectors.load_word2vec_format(filename, binary=True)

    def retrain(self, model, corpus, sg=0):
        for i in range(0, self.iter):
            model.train(corpus, total_examples=model.corpus_count)
        self.model = model



class Word2VecEmbeddingCreator(object):

    """
    A class that transforms a data into their representation of word embedding
    It uses a trained word2vec model model to build a 3 dimentional vector representation of the document.
     The first dimension represents the document, the second dimension represents the word and the third dimension is the word embedding array
    """

    def __init__(self, word2vecModel, embeddingSize=200):
        self.word2vecModel = word2vecModel
        self.embeddingSize = embeddingSize
        self.num_docs = 0


    def create_x(self, d, max_words=0):
        """
        Transform a doc2vec.TaggedDocument words into a third dimensional array
        :param d: doc2vec.TaggedDocument of a document
        :param max_words: the max number of words to put into the 3 dimensional array
        :return: the 3 dimensional array representing the content of the document
        """
        x = np.zeros(shape=(1, max_words, self.embeddingSize), dtype='float')
        for helper2, w in enumerate(d.words):
            if max_words != 0 and helper2 >= max_words:
                break
            try:
                x[0, helper2] = self.word2vecModel[w]
            except:
                x[0, helper2] = np.zeros(shape=self.embeddingSize)
        return x

    def create_x_text(self, text, max_words=0):
        """
        Transform a tokenized text into a 2 dimensional array with the word2vec model
        :param text: the tokenized text
        :param max_words: the max number of words to put into the 3 dimensional array
        :return: the 3 dimensional array representing the content of the tokenized text
        """
        x = [[]]
        # x = np.zeros(shape=(1, max_words if max_words != 0 else None, self.embeddingSize), dtype='float')
        # print(x.shape)
        for helper2, w in enumerate(text):
            if max_words != 0 and helper2 >= max_words:
                # print("breaking")
                break
            try:
                x[0].append(self.word2vecModel[w])
            except:
                x[0].append(np.zeros(shape=self.embeddingSize))
        return x

