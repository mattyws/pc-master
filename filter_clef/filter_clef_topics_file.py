import json

from bs4 import BeautifulSoup as bs
import xml.etree.ElementTree as ET

from filter_clef import config


def filter_patent_file(file_name):
    file_path = config.CLEF_TEST_FILES_PATH
    file = file_path+'/'+file_name

    tree = ET.parse(file)
    root = tree.getroot()
    claims = root.findall('claims')
    descriptions = root.findall('description')
    technical_data = root.find('bibliographic-data').find('technical-data')

    english_claim = None
    english_description = None
    # Processing claims
    for claim in claims:
        if claim.get('lang').lower() == 'en':
            english_claim = config.process_claims(claim)
            if english_claim is None:
                break
    for description in descriptions:
        if description.get('lang').lower() == 'en':
            english_description = config.process_description(description)
            if len(english_description) == 0:
                english_description = None
                break
    title = config.get_patent_title(technical_data)
    # Start the dictionary construction
    if (english_claim is not None) and (english_description is not None):
        patent = dict()
        patent['filename'] = file_name
        patent['title'] = title
        patent['description'] = english_description
        patent['claims'] = english_claim
        return patent
    else:
        return None


topics_file = open(config.CLEF_TEST_TOPICS_FILE)
topics = bs(topics_file)

errors = 0
files_processed = 0
patents = dict()
for topic in topics.find_all('topic'):
    print(topic.file.string, topic.subclass.string)
    patent = filter_patent_file(topic.file.string)
    files_processed += 1
    if patent is not None:
        if patent['filename'] in patents.keys():
            patents[patent['filename']]['ipc_classification'].append(topic.subclass.string)
        else:
            patent['ipc_classification'] = [topic.subclass.string]
            patents[patent['filename']] = patent
    else:
        errors += 1
        print(topic.file.string + " error")

patents_array = []
for key in patents.keys():
    print(key)
    patents_array.append(patents[key])
with open(config.JSON_TEST_FILES_PATH + '/test_patents.json',
          'w') as json_patent_file:
    json.dump(patents_array, json_patent_file)

print(str(errors) + " errors")
print(str(files_processed) + " total")