from pathlib import Path
from resources import text_pre_process

WORD2VEC_MODEL_PATH = "/home/mattyws/Downloads/CLEF-2011/word2vec.model"
JSON_FILES_PATH_A = "/home/mattyws/Downloads/CLEF-2011/Data/A"
JSON_FILES_PATH_B = "/home/mattyws/Downloads/CLEF-2011/Data/B"
JSON_FILES_PATH_EMBEDDING_A = "/home/mattyws/Downloads/CLEF-2011/Data/A_embedding"
JSON_FILES_PATH_EMBEDDING_B = "/home/mattyws/Downloads/CLEF-2011/Data/B_embedding"
CLEF_FILES_PATH = "/home/mattyws/Downloads/CLEF-2011/Data/clef-ip-2012-ep0/EP"
VISITED_FILE = "/home/mattyws/Downloads/CLEF-2011/Data/clef-ip-2012-ep0/visited"
DIR_VISITED_FILE = "/home/mattyws/Downloads/CLEF-2011/Data/clef-ip-2012-ep0/dir_visited"
DATASET_FILE = "/home/mattyws/Downloads/CLEF-2011/Data/clef-ip-2012-ep0/dataset"
DATASET_VISITED_FILES = "/home/mattyws/Downloads/CLEF-2011/Data/clef-ip-2012-ep0/dataset_visited"
JSON_VISITED_FILES = "/home/mattyws/Downloads/CLEF-2011/Data/clef-ip-2012-ep0/json_visited"
JSON_VISITED_DOCUMENTS = "/home/mattyws/Downloads/CLEF-2011/Data/clef-ip-2012-ep0/json_visited_documents"

CLEF_TEST_TOPICS_FILE = "/home/mattyws/Downloads/CLEF-2011/CLS_topics/task2/CLS2_en_topics.txt"
CLEF_TEST_FILES_PATH = "/home/mattyws/Downloads/CLEF-2011/CLS_topics/task2/files"
JSON_TEST_FILES_PATH = "/home/mattyws/Downloads/CLEF-2011/Data/test"
JSON_TEST_EMBEDDING_FILES_PATH = "/home/mattyws/Downloads/CLEF-2011/Data/test_embedding"

def process_claims(xml_claims):
    claims = []
    for claim in xml_claims.iter('claim'):
        for claimText in claim.iter('claim-text'):
            if claimText.text is not None:
                claims.append(claimText.text.strip().replace('\n', ' '))
    return claims


def process_description(xml_description):
    description = ""
    for p in xml_description.iter("p"):
        if p.text is not None:
            description += p.text.strip().replace('\n', ' ') + '\n'
    return description.strip()


def get_patent_classifications(technicalData):
    classifications = []
    for xml_classification in technicalData.iter('classification-ipcr'):
        classification = dict()
        classification["complete"] = xml_classification.text.replace('\t', ' ').strip()
        # classification["section"] = xml_classification.text[0]
        # classification["class"] = xml_classification.text[:3]
        # classification["subclass"] = xml_classification.text[:4]
        classifications.append(classification)
    return classifications



def get_patent_title(technicalData):
    patent_title = None
    for title in technicalData.iter('invention-title'):
        if title.get('lang').lower() == 'en':
            patent_title = title.text
    return patent_title


def get_visited_files(filename):
    my_file = Path(filename)
    if not my_file.exists():
        my_file.touch()
    with open(filename, 'r') as f:
        return f.read().split('\n')

def visited_files_to_dict(files_list):
    visited_dict = dict()
    for file in files_list:
        if file != "":
            if '/' in file:
                file = file.split('/')[-1]
            # print(file)
            file_number = file.split('-')[1]
            file_type = file.split('-')[2]
            number_first = file_number[0:len(file_number)-6]
            number_second = file_number[len(file_number)-6: len(file_number)-4]
            number_third = file_number[len(file_number)-4: len(file_number)-2]
            number_fourth = file_number[len(file_number) - 2: len(file_number)]
            # print(number_first, number_second, number_third, number_fourth)
            if number_first not in visited_dict.keys():
                visited_dict[number_first] = dict()
            if number_second not in visited_dict[number_first].keys():
                visited_dict[number_first][number_second] = dict()
            if number_third not in visited_dict[number_first][number_second].keys():
                visited_dict[number_first][number_second][number_third] = dict()
            if number_fourth not in visited_dict[number_first][number_second][number_third].keys():
                visited_dict[number_first][number_second][number_third][number_fourth] = []
            visited_dict[number_first][number_second][number_third][number_fourth].append(file_type)
    return visited_dict

def add_in(file, visited_dict):
    if file != "":
        if '/' in file:
            file = file.split('/')[-1]
        file_number = file.split('-')[1]
        file_type = file.split('-')[2]
        number_first = file_number[0:len(file_number) - 6]
        number_second = file_number[len(file_number) - 6: len(file_number) - 4]
        number_third = file_number[len(file_number) - 4: len(file_number) - 2]
        number_fourth = file_number[len(file_number) - 2: len(file_number)]
        # print(number_first, number_second, number_third, number_fourth)
        if number_first not in visited_dict.keys():
            visited_dict[number_first] = dict()
        if number_second not in visited_dict[number_first].keys():
            visited_dict[number_first][number_second] = dict()
        if number_third not in visited_dict[number_first][number_second].keys():
            visited_dict[number_first][number_second][number_third] = dict()
        if number_fourth not in visited_dict[number_first][number_second][number_third].keys():
            visited_dict[number_first][number_second][number_third][number_fourth] = []
        visited_dict[number_first][number_second][number_third][number_fourth].append(file_type)


# EP-0534648-A1.xml
def is_in(file_name, visited_dict):
    if file_name != "":
        if len(visited_dict) == 0:
            return False
        if '/' in file_name:
            file_name = file_name.split('/')[-1]
        file_number = file_name.split('-')[1]
        file_type = file_name.split('-')[2]
        number_first = file_number[0:len(file_number) - 6]
        number_second = file_number[len(file_number) - 6: len(file_number) - 4]
        number_third = file_number[len(file_number) - 4: len(file_number) - 2]
        number_fourth = file_number[len(file_number) - 2: len(file_number)]
        try:
            if file_type in visited_dict[number_first][number_second][number_third][number_fourth]:
                return True
            return False
        except:
            return False
    else:
        return True


def clean(text, tokenizer, stop_set=None, stemmer=None, remove_digits=False):
    text = text_pre_process.Tokenize(tokenizer).tokenize(text)
    if stop_set is not None:
        text = text_pre_process.CleanStopWords(stop_set).clean(text)
    if stemmer is not None:
        text = [stemmer.stem(word) for word in text]
    if remove_digits is True:
        text = [word for word in text if not word.isdigit()]
    return text