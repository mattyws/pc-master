import json

import os

from filter_clef import config
import xml.etree.ElementTree as ET
from langdetect import detect


def get_file_number(file_type):
    path = ""
    if file_type == 'A':
        path = config.JSON_FILES_PATH_A
    elif file_type == 'B':
        path = config.JSON_FILES_PATH_B
    greater = 0
    if path != "":
        files = os.listdir(path)
        for file in files:
            try:
                if greater < int(file.split('.')[0].split('-')[1]):
                    greater = int(file.split('.')[0].split('-')[1])
            except:
                pass
    return greater


max_files_per_batch = 100
dataset_files = config.get_visited_files(config.DATASET_FILE)
print(str(len(dataset_files)) + " files to process.")
dataset_visited_files = config.get_visited_files(config.DATASET_VISITED_FILES)
dataset_visited_dict = config.visited_files_to_dict(dataset_visited_files)
A_patent_files = []
B_patent_files = []
with open(config.DATASET_VISITED_FILES, 'a', 1) as dataset_visited_handler:
    i = 0
    A_num = get_file_number('A')
    B_num = get_file_number('B')
    for file in dataset_files:
        if len(A_patent_files) >= max_files_per_batch:
            # Writing batch file
            with open(config.JSON_FILES_PATH_A + '/A-' + str(A_num) + '.json', 'w') as f:
                json.dump(A_patent_files, f)
            for A_file in A_patent_files:
                config.add_in(A_file['filename'], dataset_visited_dict)
                dataset_visited_handler.write(A_file['filename'] + '\n')
            A_patent_files = []
            A_num += 1
        if len(B_patent_files) >= max_files_per_batch:
            # Writing batch file
            with open(config.JSON_FILES_PATH_B + '/B-' + str(B_num) + '.json', 'w') as f:
                json.dump(B_patent_files, f)
            for B_file in B_patent_files:
                config.add_in(B_file['filename'], dataset_visited_dict)
                dataset_visited_handler.write(B_file['filename'] + '\n')
            B_patent_files = []
            B_num += 1

        file_name = file.split('/')[-1]
        if not config.is_in(file_name, dataset_visited_dict):
            # Start xml processing
            tree = ET.parse(file)
            root = tree.getroot()
            claims = root.findall('claims')
            descriptions = root.findall('description')
            technicalData = root.find('bibliographic-data').find('technical-data')

            english_claim = None
            english_description = None
            document_technicalData = None
            # Processing claims
            for claim in claims:
                if claim.get('lang').lower() == 'en':
                    english_claim = config.process_claims(claim)
                    if english_claim is not None and len(english_claim) > 0:

                        '''
                        Checking if language is really english. The dataset have some documents using 'en' as language
                        with text writed in other language (FR or DE). If TextBlob accuse any text to not be writed in english,
                        discard all claims.
                        '''
                        text = '\n'.join(english_claim)
                        if len(text) > 3:
                            try:
                                #Using TextBlob
                                # lang = TextBlob(text).detect_language().lower()
                                # Using langdetect
                                lang = detect(text).lower()
                                if not lang == 'en':
                                    english_claim = None
                            except:
                                english_claim = None
                    if english_claim is None:
                        break

            # Processing descriptions
            for description in descriptions:
                if description.get('lang').lower() == 'en':
                    english_description = config.process_description(description)
                    if len(english_description) == 0:
                        english_description = None
                    else:
                        '''
                        Checking if language is really english. The dataset have some documents using 'en' as language
                        with text writed in other language (FR or DE). If TextBlob accuse any text to not be writed in english,
                        discard the description.
                        '''
                        try:
                            # Using TextBlob
                            # lang = TextBlob(english_description).detect_language().lower()
                            # Using langdetect
                            lang = detect(english_description).lower()
                            if not lang == 'en':
                                english_description = None
                        except:
                            english_description = None
                    if english_description is None:
                        break
            classifications = config.get_patent_classifications(technicalData)
            title = config.get_patent_title(technicalData)
            # Start the dictionary construction
            if (len(classifications) > 0) and (english_claim is not None) and (english_description is not None):
                patent = dict()
                patent['filename'] = file_name
                patent['title'] = title
                patent['description'] = english_description
                patent['claims'] = english_claim
                patent['ipc_classification'] = classifications
                if 'B' in file:
                    B_patent_files.append(patent)
                elif 'A' in file:
                    A_patent_files.append(patent)
            else:
                # Adding file to consumed if its not english
                print(file_name + " is not english")
                config.add_in(file_name, dataset_visited_dict)
                dataset_visited_handler.write(file_name + '\n')
        i += 1
        if i % 100 == 0:
            print("Visited " + str(i) + " files.",
                  "A size: " + str(len(A_patent_files)),
                  " B size: " + str(len(B_patent_files)))
    if len(A_patent_files) >= 0:
        # Writing batch file
        with open(config.JSON_FILES_PATH_A + '/A-' + str(A_num) + '.json', 'w') as f:
            json.dump(A_patent_files, f)
        for A_file in A_patent_files:
            config.add_in(A_file['filename'], dataset_visited_dict)
            dataset_visited_handler.write(A_file['filename'] + '\n')
        A_patent_files = []
        A_num += 1
    if len(B_patent_files) >= 0:
        # Writing batch file
        with open(config.JSON_FILES_PATH_B + '/B-' + str(B_num) + '.json', 'w') as f:
            json.dump(B_patent_files, f)
        for B_file in B_patent_files:
            config.add_in(B_file['filename'], dataset_visited_dict)
            dataset_visited_handler.write(B_file['filename'] + '\n')
        B_patent_files = []
        B_num += 1