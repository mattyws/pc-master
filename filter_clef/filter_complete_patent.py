import os

from filter_clef import config
import xml.etree.ElementTree as ET



max_files_per_batch = 100
A_num = 0
B_num = 0
A_patent_files = []
B_patent_files = []
visited_files = config.get_visited_files(config.VISITED_FILE)
visited_dict = config.visited_files_to_dict(visited_files)
dataset_files = config.get_visited_files(config.DATASET_FILE)
dataset_dict = config.visited_files_to_dict(dataset_files)

# dir_visited_files = config.get_visited_files(config.DIR_VISITED_FILE)
num_iterations = 0
with open(config.VISITED_FILE, 'a', 1) as visited_file_handler, \
        open(config.DATASET_FILE, 'a', 1) as dataset_file_handler, \
        open(config.DIR_VISITED_FILE, 'a', 1) as dir_visited_file_handler:
    i = 0
    print("Start processing patent files, it may take while to catch up with the process.")
    for dir, subdir, files in os.walk(config.CLEF_FILES_PATH):
        if len(files) > 0:
            for file in files:
                if ".xml" in file:
                    if not config.is_in(file, visited_dict):
                        try:
                            #Start xml processing
                            tree = ET.parse(dir+"/"+file)
                            root = tree.getroot()
                            claims = None
                            descriptions = None
                            abstracts = None
                            technicalData = None
                            classification_icpr = None
                            if len(root.findall('claims')) > 0:
                                claims = root.findall('claims')
                            if len(root.findall('description')) > 0 :
                                descriptions = root.findall('description')
                            if root.find('bibliographic-data').find('technical-data') is not None:
                                technicalData = root.find('bibliographic-data').find('technical-data')
                            if (claims is not None) and (descriptions is not None) and (technicalData is not None):
                                english_claims = False
                                english_descriptions = False
                                for claim in claims:
                                    if claim.get('lang').lower() == 'en':
                                        english_claims = True
                                for description in descriptions:
                                    if description.get('lang').lower() == 'en':
                                        english_descriptions= True
                                if english_claims and english_descriptions and not config.is_in(file, dataset_dict):
                                    config.add_in(file, dataset_dict)
                                    dataset_file_handler.write(dir+'/'+file+'\n')
                            # Adding file to consumed and wrinting to the end of the file
                            config.add_in(file, visited_dict)
                            visited_file_handler.write(file + '\n')
                            i += 1
                            if i % 1000 == 0:
                                print("Visited " + str(i) + " files.")
                        except:
                            print(dir+'/'+file + " error while parsing.")
        num_iterations += 1
        if num_iterations % 5000 == 0:
            print("Iterations: " + str(num_iterations))