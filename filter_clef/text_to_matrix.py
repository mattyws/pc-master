import json
import os
import pickle
import pprint

import nltk

from resources import data_generators
from filter_clef import config
from resources.data_representation import Word2VecTrainer, Word2VecEmbeddingCreator


def get_file_number(path):
    greater = 0
    if path != "":
        files = os.listdir(path)
        for file in files:
            try:
                if greater < int(file.split('.')[0].split('-')[1]):
                    greater = int(file.split('.')[0].split('-')[1])
            except:
                pass
    return greater


def to_embedding(text, tokenizer, stop_set, word_embedding_creator):
    text = config.clean(text, tokenizer, stop_set=stop_set)
    return word_embedding_creator.create_x_text(text)


def transform(documents_in_json, tokenizer, stop_set, word_embedding_creator):
    documents_new_representation = []
    for document in documents_in_json:
        new_document = dict()
        new_document['filename'] = document['filename']
        new_document['title'] = document['title']
        new_document['ipc_classification'] = document['ipc_classification']
        new_document['description_embedding'] = to_embedding(document['description'], tokenizer, stop_set, word_embedding_creator)
        new_document['claims_embedding'] = []
        for claim in document['claims']:
            new_document['claims_embedding'].append(to_embedding(claim, tokenizer, stop_set, word_embedding_creator))
        documents_new_representation.append(new_document)
    return documents_new_representation

language = 'english'
tokenizer = nltk.tokenize.RegexpTokenizer(r'\w+')
stop_set = nltk.corpus.stopwords.words(language)

word2vec_model = Word2VecTrainer().load_model(config.WORD2VEC_MODEL_PATH)
word_embedding_creator = Word2VecEmbeddingCreator(word2vec_model, embeddingSize=0)

path = config.JSON_TEST_FILES_PATH
path_embedding = config.JSON_TEST_EMBEDDING_FILES_PATH
generator = data_generators.PatentDataGenerator(path, loop_forever=False, max_documents_per_file=1000)
json_visited_files = config.get_visited_files(config.JSON_VISITED_FILES)

json_document_name = ""
documents_in_json = []
with open(config.JSON_VISITED_FILES, 'a') as json_visited_file_handler:
    i = 0
    for document in generator:
        i+=1
        print(i)
        # pprint.PrettyPrinter(indent=4).pprint(document)
        # break
        document_in_json_file = generator.get_json_file_name()
        if document_in_json_file not in json_visited_files:
            if json_document_name == "":
                json_document_name = document_in_json_file
            if json_document_name != document_in_json_file:
                print("Transforming " + json_document_name)
                transformed_documents = transform(documents_in_json, tokenizer, stop_set, word_embedding_creator)
                print("Writing " + json_document_name)
                with open(path_embedding+"/"+json_document_name+'.pkl', "w") as file:
                    pickle.dump(transformed_documents, file)
                json_visited_file_handler.write(json_document_name+'\n')
                json_visited_files.append(json_document_name)
                json_document_name = document_in_json_file
                documents_in_json = [document]
                # break
            else:
                documents_in_json.append(document)
        else:
            print(document_in_json_file + " visited")
            generator.advance_json_file()
    print("Transforming " + json_document_name)
    transformed_documents = transform(documents_in_json, tokenizer, stop_set, word_embedding_creator)
    print("Writing " + json_document_name)
    with open(path_embedding + "/" + json_document_name+'.pkl', "w") as file:
        pickle.dump(transformed_documents, file)